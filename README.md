# Dockerized Playground

A docker multicontainer with Nginx, PHP, Postgres and Redis.
This multicontainer project will give you access to the following empty projects :
* AngularJS
* Laravel
* Ruby on Rails
* ReactJS
* Symfony
* VueJS

## Getting started

To make it easy for you to get started with Gitlab and this project, here is a list of recommended next steps.

Already a pro? Just edit this README.md file and make it your own!

### Grab these files

- [ ] [Download archive](https://gitlab.com/Veituselah/dockerized-playground/-/archive/master/dockerized-playground-master.zip) of this project, or clone it using Git :

```
git@gitlab.com:Veituselah/dockerized-playground.git
```

- [ ] Pull it to an existing repository of your own with the following command :

```
cd existing_empty_repo
git pull https://gitlab.com/Veituselah/dockerized-playground.git
```

### Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:54db0a37e09824913f6243f6fef0f2d0?https://docs.gitlab.com/ee/user/project/integrations/)

### Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:54db0a37e09824913f6243f6fef0f2d0?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:54db0a37e09824913f6243f6fef0f2d0?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:54db0a37e09824913f6243f6fef0f2d0?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:54db0a37e09824913f6243f6fef0f2d0?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

### Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:54db0a37e09824913f6243f6fef0f2d0?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:54db0a37e09824913f6243f6fef0f2d0?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:54db0a37e09824913f6243f6fef0f2d0?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:54db0a37e09824913f6243f6fef0f2d0?https://docs.gitlab.com/ee/user/clusters/agent/)


## How to Use

> WARNING: Before making use of these following instructions, please consider that default user of this package is ***www-data***. To change this default username, please take a look at Nginx ***Dockerfile*** & ***nginx.conf***, Php8-fpm ***Dockerfile*** & ***www.conf*** files.

1. Build and run containers

    ```
    docker-compose build
    docker-compose up -d
    ```
> To show logs, remove the -d argument

2. Deploy Composer

    ```
    docker-compose exec -u www-data php composer install
    ```

### Build specific services

1. Build nginx service

    ```
    docker-compose build nginx
    ```

2. Build php service

    ```
    docker-compose build php
    ```

### Enter containers

1. Enter Php-fpm container

    ```
    docker-compose exec -u www-data php sh
    ```
> Run whatever you want (E.g. 'composer require').

<!-- ## Run an AngularJS project -->
## Run a Laravel project

1. Generate your 'APP_KEY'

    ```
    docker-compose exec -u www-data php php artisan key:generate
    ```

>To do it manually, please use 'docker-compose exec -u www-data php sh' followed by 'php artisan key:generate'.

2. Run your project

    ```
    docker-compose exec -u www-data php php artisan serve
    ```

<!-- ## Run a Ruby on Rails project
## Run a ReactJS project
## Run a Symfony project
## Run a VueJS project -->

## Troubleshooting

You may face some issues during previous manipulations. We thought these few lines can help you.

### Permissions

If you have problems with permissions on files, try to run command:

    ```
	docker-compose up -d
	docker-compose exec php sh -c 'usermod -u ${UID} www-data'
	docker-compose stop
    ```

>UID=$(shell id -u)
>Perhaps your user has a UID other than 1000

### Xdebug

Feel free to change xdebug config in [xdebug.ini](docker/php8-fpm/xdebug.ini) (For example `xdebug.idekey` if you are not using PHPStorm).